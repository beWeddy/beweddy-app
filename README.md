# beWeddy-App



System ma oferować pomoc dla osób przygotowujących wesele. 
System będzie posiadał dwa typy użytkowników: Zespoły (usługi) oraz zwykli użytkownicy (organizatorzy).  
Po zalogowaniu się zwykły użytkownik będzie mógł przeglądać oferty zespołów wraz z terminami w których są dostępne.

Celem głównym naszego projektu jest wpasowanie się w niszę, 
którą jest organizacja przyjęć weselnych pod względem doboru zespołu muzycznego, 
a następnie – z biegiem czasu – również innych usług, miejsc i atrakcji dostosowanych pod życzenia klienta. 
Przy założeniu, że pomysł odniesie sukces – mamy ogromne szanse na zostanie liderem na tym rynku.

Celem bezpośrednim jest ekspansja na rynku poprzez stopniowe wdrażanie nowych funkcjonalności
oraz poszerzanie systemu o możliwość organizacji większej ilości przyjęć tematycznych, 
co równocześnie wiąże się z napływem klientów i generowaniem większego dochodu. 
Pierwszym krokiem milowym jest zarobienie miliona złotych.

Pierwszą, funkcjonalną wersję systemu zamierzamy wdrożyć do końca stycznia 2019r. 
Będzie ona promowała ułatwianie organizacji przyjęć weselnych, skupiając się na zespołach muzycznych – z tej racji, 
pierwsze akcje marketingowe kierowane będą do potencjalnych par młodych, poprzez reklamy internetowe, ulotki i drogę słowną.

Dodatkowe funkcjonalności, takie jak możliwość rezerwacji miejsca przyjęcia czy usług gastronomicznych, 
będą wdrażane wraz z upływem czasu i rozrostem bazy użytkowników systemu. 
Początkowo głównym źródłem dochodu będą pieniądze generowane przez reklamy i możliwość ich (płatnego) wyłączenia, 
oraz serwis na bazie subskrypcji dla zespołów, dzięki któremu będą one częściej pojawiać się w wyszukiwaniach. 
Z czasem wprowadzone zostaną inne usługi premium generujące przychód, i ułatwiające użytkownikom korzystanie z systemu.

Domyślnie istnieją dwa typy użytkowników:

Organizator – osoba(y) organizująca przyjęcie, która poszukuje
     zespołu muzycznego, najczęściej Para młoda

Usługa  --  użytkownik promujący swoją działalność, 
który wyznaczając terminy dostępności stara się znaleźć potencjalnego klienta (Parę młodą)

Użycie takich typów użytkowników pozwoli na ujednolicenie systemu
i łatwiejszą integrację dodawanych funkcjonalności.
